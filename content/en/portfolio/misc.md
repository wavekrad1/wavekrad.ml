---
author: "wavekrad"
title: "Miscellaneous Archives"
date: 2021-05-19T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "A collection of my various works from over the years."
image: images/thumbnails/misc.png
tags: 
- portfolio
---

{{< img src="/images/content/misc1.png" width="700px" position="center" >}}

{{< img src="/images/content/misc2.png" width="700px" position="center" >}}

{{< img src="/images/content/misc3.png" width="700px" position="center" >}}

{{< img src="/images/content/misc4.png" width="700px" position="center" >}}

{{< img src="/images/content/misc5.png" width="700px" position="center" >}}