---
author: "wavekrad"
title: "Cinema Vectors"
date: 2021-04-11T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "A small suite of cinema themed vector images I created."
image: images/thumbnails/vectorsPromoPoster.png
tags: 
- portfolio
---

A popcorn bucket, film camera and a pair of 3D glasses. These are the most prominent pieces of film. When designing a poster for a fictituous upcoming movie theater, I knew I had to incorporate them somehow.

{{< img src="/images/thumbnails/vectorsPromoPoster.png" width="400px" position="center" >}} 

To create these, I took an interesting approach. I started off by creating 3D models of each item in Blender, then using those as a refrence in InkScape. This in turn, helped me create these assets completely independantly.

{{< img src="/images/content/cinemarender.png" width="400px" position="center" >}} 