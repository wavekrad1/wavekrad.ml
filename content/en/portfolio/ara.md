---
author: "wavekrad"
title: "ARA Evolution"
date: 2021-05-13T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "A suite of promotional material for a small cosmetic business."
image: images/thumbnails/ara.png
tags: 
- portfolio
---

{{< img src="/images/content/aracontent.png" caption="Made in Krita" width="700px" position="center" >}}

ARA Evolution is a small but growing business, that offers cosmetic glitter. Their charm is that their products are completely biodegradable and eco-friendly. The images above were made by me to help spicen their brand, using wooden textures to reflect on their environmental nature.

The folks are up to good stuff, check out their products <a href="https://www.etsy.com/uk/shop/araevolution/">here!</a>