---
author: "wavekrad"
title: "DR1 Handgun Test"
date: 2021-04-15T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "Some Handgun animations, inspired by Doom and Dead Rising."
image: images/thumbnails/dr1handgun.png
tags: 
- portfolio
---

{{< yewtube znpBn9Q9p3w >}}

Here's a short video, showing off some sprite animations I did. The premise was to bring Dead Rising to the Doom artstyle, with retro graphics. As such, the weapon and character sleeve was made using Dead Rising's original assets. Made in Krita and Blender.