---
author: "wavekrad"
title: "CataclysmEXPO"
date: 2021-04-12T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "Some assets I designed for a fictitious Video Game convention."
image: images/thumbnails/cataclysmexpo.png
tags: 
- portfolio
---

{{< img src="/images/content/cataclysmexpo.png" caption="Made in Krita" width="700px" position="center" >}}

With a resurgence of Retro First Person Shooter styled video games, thanks to open-source game engines such as GZDoom, the idea of a convention centered around the genre is exciting! In fact, 3D Realms hosted their "Realms Deep 2020" digital convention dedicated to upcoming retro FPS titles. The artwork I made above would be used for a variety of applicants: leaflets, banners, merchandise and other means of advertisement. The aesthetic is heavily reflected on games from the genre, such as the Doom series and Duke Nukem 3D.