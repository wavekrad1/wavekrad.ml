---
author: "wavekrad"
title: "Frank West"
date: 2021-04-11T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "Here's a collection of sprites I did, based on Capcom's Frank West character."
image: images/thumbnails/frankwestdoom.png
tags: 
- portfolio
---

{{< img src="/images/content/frankwestdoom.png" caption="Made in Krita" width="700px" position="center" >}}

Pixel art is an art format that, for the longest time, I struggled with. In January 2021, I wanted to push my creative abilities and overcome this struggle by recreating Frank West from the game Dead Rising in the style. I created several iterations of our friend, Frank, to show him expressing different emotions. As he becomes more bloody and bruised, it appears he has become more worrisome and fearful for his life. I took major inspiration from the video game Doom 2, which sees it's main character in an array of similar expressions and progressive demise.
