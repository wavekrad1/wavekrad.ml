---
author: "wavekrad"
title: "Ditching Chromium Browsers for LibreWolf"
date: 2021-04-28T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "Librewolf is a well maintained fork of Firefox that you might like."
image: images/thumbnails/librewolf.png
tags: 
- blog
---

For almost a year now, I've been bouncing around different browsers. I found myself usually sticking to Chromium based ones, like Brave and Dissenter. However as I began to grow out of these Chrome forks, the vast amount of Firefox based browsers began to pique my interest. Things like Pale Moon, GNU IceCat, WaterFox and others seemed very interesting. However, I never really found my footing with them and I found myself always going back to Dissenter. This was until I tried out LibreWolf, which ultimately, I think I'll be staying with.

{{< img src="/images/content/librewolf2.png" width="700px" position="center" >}}<br>
LibreWolf is a community built, open-source Firefox fork that centers itself around privacy and security for its users. Out of the box, it comes with some great features like stripping out any telemetry and it also includes Ad-Block out of convenience. This, paired with some extra Firefox addons you can install manually, sets you up for a very secure browser. It also includes some really cool fingerprint resistance tools. When I visit websites now, they log me as a Windows 32 Bit User, when I'm actually on Linux.

{{< img src="/images/content/librewolf1.png" width="700px" position="center" >}}<br>
If you want to install LibreWolf for yourself, the steps you'll have to take is dependant on your Operating System. If you're on a Linux distrobution with access to the AUR, you can clone the binary release of it relatively easily. If you're on another distro, or using Mac / Windows, you'll have to snoop around on their <a href="https://gitlab.com/librewolf-community">GitLab</a> page. 