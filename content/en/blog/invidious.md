---
author: "wavekrad"
title: "Embedding Invidious videos with Hugo"
date: 2021-04-11T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "Embedding YouTube videos without trackers on your website? It's possible."
image: images/thumbnails/invidious.png
tags: 
- blog
---

YouTube is one of the most popular video hosting sites ever, and it's been that way for a while. However, it's business strategy relies on tracking various information on it's users. This includes their location, search history and other identifying data that can be sold to advertisers. Luckily, the folks over at Invidious have a solution.

Using one of Invidious' mirrors, you are able to view and watch YouTube videos without trackers or even advertisements. It also allows you to download videos if you so wish. You can also embed YouTube videos under Invidious' front-end. This is important if you want to share video content on your website without affecting your viewers. If your website is built with HTML, you can easily find the video you want and click "Embed". However if you're like me, and your website is built in Hugo or something similar, there's another way to do this. Infact it's a lot simpler.

First, from the root of your website, navigate to `layouts/shortcodes` (create the 'shortcodes' folder if it doesn't exist) and create a html file called `yewtube.html`. Then paste the following:

```
<div class="embed video-player">
<iframe id="ivplayer" width="640" height="360" src="https://yewtu.be/embed/{{ index .Params 0 }}" style="border:none;"></iframe>
</div>
```

Now, let's say I want to embed the video in this link `https://www.youtube.com/watch?v=9h1IPA0s6D0`, I can do 
`((< yewtube 9h1IPA0s6D0 >))` (replace the brackets with {urly ones). like so:

{{< yewtube 9h1IPA0s6D0 >}}

<center>
Not bad, huh? Hope this helps!
</center>