---
author: "wavekrad"
title: "Materials, Techniques and Processes"
date: 2021-04-11T12:00:06+09:00
draft: false
hideToc: false
enableToc: true
enableTocContent: false
author: wavekrad
mode: "at-once" # at-once is a default value
description: "N/A"
image: images/thumbnails/misc.png
tags: 
- blog
---

{{< embed-pdf url="./static/misc/draft2.pdf" >}}