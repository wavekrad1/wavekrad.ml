+++
title = "About"
description = "It's wavekrad."
type = "about"
date = "2019-02-28"
+++

Hello, my name is Cameron. I am currently studying Graphic and Digital design in Norwich. Graphic Design is a huge passion of mine, and my focus is directed at satisfying the artistic visions of my clients. Since the beginning of the year, I have made the abrupt decision to go Open-Source; using Libre software on my GNU/Linux machine to design my artwork as much as I can. 

I try to keep my portfolio as diverse as possible, learning new things as I mature as a creative. Vectors, 3D, Pixel Art, Video Editing, Printing, Motion Graphics, there's something for everyone to enjoy at wavekrad. If you are interested in my services, check out the Portfolio tab.

Thank you for viewing my website, feel free to contact me directly over at wavekrad@gmx.com

P.S. This website was built with Hugo, and the source code is available over at my GitLab page, which can be found in the footer!



